import { Bar, Line } from 'react-chartjs-2';
import { Container } from 'reactstrap'
import { connect } from 'react-redux'

// Chart terima state (data, label) sebagai props lewat HOC (Higher Order Component) connect
const Chart = ({ data, label }) => {

  const options = {
    labels: label,
    datasets: [{
      label: 'Ini Warna',
      data,
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 205, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(201, 203, 207, 0.2)'
      ],
      borderColor: [
        'rgb(255, 99, 132)',
        'rgb(255, 159, 64)',
        'rgb(255, 205, 86)',
        'rgb(75, 192, 192)',
        'rgb(54, 162, 235)',
        'rgb(153, 102, 255)',
        'rgb(201, 203, 207)'
      ],
      borderWidth: 1
    }]
  }

  return (
    <Container>
      <div className="py-5">
        <Bar 
          data={options}
        />
      </div>
      <div className="py-5">
        <Line data={options}/>
      </div>
    </Container>
  )
}

// Ini Tugasnya untuk ngasih komponen Chart ini sebuah props yang isinya adalah state
const mapStateToProps = state => {
  return {
    data: state.data,
    label: state.label
  }
}

// Higher Order Component connect

export default connect(mapStateToProps)(Chart)

import { Container } from 'reactstrap'
import ReactECharts from 'echarts-for-react'
import { connect } from 'react-redux'




const EChart = ({ data, label }) => {
  const barOption = {
    xAxis: {
      type: 'category',
      data: label
    },
    yAxis: {
      type: 'value'
    },
    series: [{
      data,
      type: 'bar'
    }]
  }

  const lineOption = {
    xAxis: {
      type: 'category',
      data: label
    },
    yAxis: {
      type: 'value'
    },
    series: [{
      data,
      type: 'line'
    }]
  }
  return (
    <Container>
      <ReactECharts option={barOption}/>
      <ReactECharts option={lineOption}/>
    </Container>
  )
}

const mapStateToProps = state => {
  return {
    data: state.data
  }
}

export default connect(mapStateToProps)(EChart)

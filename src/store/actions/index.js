import { SET_DATA } from '../types';

// Action Creator
// Ini tugasnya cuma untuk ngirim type (wajib) dan payload (tidak wajib) ketika ada event yang berjalan untuk memanggil actionnya.
export const setData = (data) => {
  // Returnnya adalah sebuah object yang berupa action
  return {
    type: SET_DATA,
    payload: data,
  };
};

// Harus return object
// Async bakal ngubah semua return menjadi Promise, dan promise ini bukan object
// export const fetchNew = async () => {
//   const { data } = await axios.get(url);
//   return {
//     type: 'GET_DATA',
//     payload: data
//   }
// }

import { SET_DATA } from '../types';

// Reducer tugasnya untuk nyimpan state secara global dan juga untuk mengolah state sesuai dengan action yang dikirim
const initialState = {
  data: [],
  label: [],
};

// Reducer menerima state dan action sebagai argumen supaya dapat diolah
const dataReducer = (state = initialState, action) => {
  // Destructure type & payload dari object action
  const { type, payload } = action;

  // Reducer tau mau jalankan apa sesuai type yang dikirim oleh action
  switch (type) {
    case SET_DATA:
      // Ingat bahwa state itu ga boleh di ubah secara langsung, maka harus diberlakukan spread operator (...state) untuk copy state sebelumnya supaya dapat diubah
      return {
        ...state,
        data: payload.data,
        label: payload.label,
      };
    default:
      return state;
  }
};

export default dataReducer;

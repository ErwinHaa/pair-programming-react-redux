import { Link } from 'react-router-dom'
import { Navbar, Nav, NavItem } from 'reactstrap'

import './Header.css'

const Header = () => {
  return (
    <Navbar expand="lg" color="dark" dark>
      <Nav>
        <NavItem>
          <div className="nav-link">
            <Link to="/">Excel</Link>
          </div >
        </NavItem>
        <NavItem>
          <div className="nav-link">
            <Link to="/chart">Chart</Link>
          </div>
        </NavItem>
        <NavItem>
          <div className="nav-link">
            <Link to="/e-chart">EChart</Link>
          </div>
        </NavItem>
      </Nav>
    </Navbar>
  )
}

export default Header

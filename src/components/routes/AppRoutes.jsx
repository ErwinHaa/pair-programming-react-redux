import { Route, Switch } from 'react-router-dom'

import Excel from '../../pages/Excel'
import Chart from '../../pages/Chart'
import EChart from '../../pages/EChart'

const AppRoutes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Excel />
      </Route>
      <Route exact path="/chart">
        <Chart />
      </Route>
      <Route exact path="/e-chart">
        <EChart />
      </Route>
    </Switch>
  )
}

export default AppRoutes

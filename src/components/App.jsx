import AppRoutes from './routes/AppRoutes'
import Header from './Header'

const App = () => {
  return (
    <div>
      <Header />
      <AppRoutes/>
    </div>
  )
}

export default App
